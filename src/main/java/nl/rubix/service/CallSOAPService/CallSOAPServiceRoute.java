package nl.rubix.service.CallSOAPService;

import org.apache.camel.builder.RouteBuilder;

public class CallSOAPServiceRoute extends RouteBuilder{

	@Override
	public void configure() throws Exception {
		from("timer://runOnce?repeatCount=1&delay=1000").routeId("CallSOAPServiceRoute")
		.log("+++ Start instance +++")
		.processRef("newOrderProcessor")
		.log("log ${body}")
		.setHeader("OperationName", simple("getTheSpecialOrder"))
		.to("cxf:bean:newOrderEndpoint")
		.log("log ${body}")
		.processRef("processResponse")
		
		;
		
	}
	

}
